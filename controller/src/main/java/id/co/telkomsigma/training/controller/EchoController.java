package id.co.telkomsigma.training.controller;

import id.co.telkomsigma.training.dao.pojo.request.EchoRequest;
import id.co.telkomsigma.training.dao.pojo.response.BaseResponse;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;


@RestController
public class EchoController {

	public static final String TERMINAL_ECHO_PATH = "/webservice/echo**";

	@RequestMapping(value = TERMINAL_ECHO_PATH, method = RequestMethod.POST)
	public @ResponseBody
    BaseResponse echo(@RequestBody EchoRequest request) throws IOException {
		BaseResponse response = new BaseResponse();
		if (request.getRequestCode().equals("ECHO")) {
			response.setResponseCode("00");
			response.setResponseMessage("SUCCESS");
		} else {
			response.setResponseCode("01");
			response.setResponseMessage("FAILED");
		}
		return response;
	}
}