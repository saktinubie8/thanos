package id.co.telkomsigma.training.controller;

import id.co.telkomsigma.training.dao.entity.Role;
import id.co.telkomsigma.training.dao.entity.Users;
import id.co.telkomsigma.training.dao.pojo.request.AddUserRequest;
import id.co.telkomsigma.training.dao.pojo.request.UpdateUserRequest;
import id.co.telkomsigma.training.dao.pojo.response.AddUserResponse;
import id.co.telkomsigma.training.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    Set<Role> roles = new HashSet<>();

    @RequestMapping(value = "/internal/userCheck**", method = {RequestMethod.POST})
    public String login(@RequestParam String username, @RequestParam String password) {
        Users user = userService.findByUsernameAndPassword(username, password);
        if (user != null) {
            return "SUCCESS";
        } else {
            return "FAILED";
        }
    }

    @RequestMapping(value = "/webservice/addUser**", method = {RequestMethod.POST})
    public @ResponseBody
    AddUserResponse insertUser(@RequestBody AddUserRequest request) {
        AddUserResponse response = new AddUserResponse();
        String userId = "";
        Users user = new Users();
        user.setUsername(request.getUsername());
        user.setPassword(new BCryptPasswordEncoder().encode(request.getPassword()));
        Role role = userService.findByAuthority(request.getRole());
        roles.add(role);
        user.setRoles(roles);
        user.setSalary(request.getSalary());
        user.setAge(request.getAge());
        userService.insertUser(user);
        userId = user.getUserId().toString();
        response.setUserId(userId);
        response.setResponseCode("00");
        response.setResponseMessage("SUCCESS");
        return response;
    }

    @RequestMapping(value = "/webservice/updateUser**", method = {RequestMethod.POST})
    public @ResponseBody
    AddUserResponse updateUser(@RequestBody UpdateUserRequest request) {
        AddUserResponse response = new AddUserResponse();
        String userId = "";
        Users user = userService.findByUsername(request.getUsername());
        user.setUsername(request.getUsername());
        Role role = userService.findByAuthority(request.getRole());
        roles.add(role);
        user.setRoles(roles);
        userService.updateUser(user);
        userId = user.getUserId().toString();
        response.setUserId(userId);
        response.setResponseCode("00");
        response.setResponseMessage("SUCCESS");
        return response;
    }

    @RequestMapping(value = "/webservice/listUser**", method = {RequestMethod.GET})
    public List listUser(){
        return userService.findAll();
    }

}
