package id.co.telkomsigma.training.dao.entity;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

@Entity
@Table(name = "ROLES_AUTH")
public class Role implements GrantedAuthority {

    private Long roleId;
    private String authority;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue
    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    @Override
    @Column(name = "ROLE_NAME",  unique = true)
    public String getAuthority() {
        return null;
    }
}
