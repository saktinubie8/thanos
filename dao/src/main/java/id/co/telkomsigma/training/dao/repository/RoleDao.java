package id.co.telkomsigma.training.dao.repository;

import id.co.telkomsigma.training.dao.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleDao extends JpaRepository<Role, Long> {

    Role findByAuthority(String roleName);

}
