package id.co.telkomsigma.training.dao.repository;

import id.co.telkomsigma.training.dao.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDao extends JpaRepository<Users, Long> {
    Users findByUsernameAndPassword(String username, String password);

    Users findByUsername(String username);
}
